## Banshee

**DISCLAIMER - There is no legal gray-area to hide behind here; this is a 2.4Ghz Jammer. There are quite literally 0 constructive purposes for a device like this as anything other than an education tool to show how $30 will demonstrate how fragile our wireless ecosystem is. While the components themselves are not illegal, and even the components assmebled together in this fashion are not illegal, THIS CODE WILL PERFORM ILLEGAL ACTIONS. It is very apparent when the device is in use if it's targetting a specific WiFi channel, because if used in the "correct" way, it can and will completely disable all wireless communications for that network/channel.**



Before you assume this is a nuisance at best, there are two things I want to make clear.



1 - In its current state, this will barely affect Bluetooth connectivity. Bluetooth uses channel-hopping with 3 advertising channels and 37 total channels to transmit data between. There exist a plethora of other projects involving STMs and multiple NRF transceivers. This is not one of those.



2 - Wireless, in some areas, is more reliable than celluar connections. For some people, it is possibly the only thing that can be used in an emergency. Disregarding the moral implications of disallowing that access, if you are caught, it can very quickly go from a $10,000 fine to a legitimate jail sentence - especially if you are the reason someone's family member died instead of getting to a hospital.



Do not be a dumb ass. Being a part of the problem does not get the problem solved faster, it just makes the rest of us look evil. If you are interested in a similar sort of attack WITHOUT the potential of preventing a neighbor from getting help, I would strongly recommend something like Aircrack-ng, Kismet, or, my own deauth program, [Fer-De-Lance](https://gitlab.com/forrest_h1/python-scripts/networking-and-scapy-tools/malice/fer-de-lance). All of these utilize an attack vector known as deauthentication, which is in some cases less obvious and safer. 



# How does it work?



Banshee is a program written specifically for the Arduino Nano (Or, in my case, the Osoyoo LGT Nano) and the NRF24L01+. It is designed to simplify some of the existing similar jammers present in the community. There are a few jammers that either try to make the NRF into a wideband transmitter, going across the full frequency range of individual wifi channels to compensate for the 1Mhz bandwidth available - or, they seem to target off-center. Banshee works by specifically targetting the center channel (F0) of a given WiFi Channel, as through my testing, this was discovered to be the quickest and most efficient way of jamming a network. 



Upon runtime, the NRF's channel will be set based upon the selected WiFi channel. For example, if I wanted to attack any/all networks on 802.11's Ch. 1 (F0 = 2412Mhz), the channel given to the NRF will be 'Ch. 12'. The caveat of attacking the center frequency is that if you are up against a powerful AP, it will either take longer for the jammer to have full effect or you will be giving intermittent interference at best. I am working on a more effective method of jamming a network.



In my experience, placing the jammer in between a theoretical line-of-sight to the access point with the correct channel being selected will usually knock connectivity offline. **PLEASE NOTE THAT THE PA VERSION ABSOLUTELY HAS THE POTENTIAL TO KILL YOUR NEIGHBOR'S NETWORKS.**


**NOTE:** Most APs have a form of channel-Switching that is dependent upon the amount of noise detected within the frequency range of a channel. This tool, in its current state, does not actively try to detect channel switching.



# Libraries required 



-SPI


-nRF24L01



-RF24



# Pinout (Subject to change as hardware control is implemented)


| ARDUINO PIN | NRF24L01 PIN |
| --- | --- |
| D9 | CE |
| D10 | CSN |
| D11 | MOSI |
| D12 | MISO |
| D13 | SCK |
| 3V3 | VCC |
| GND | GND |



**The IRQ pin is not used here.**


# Todo



- Wifi Activity detection using a 2nd NRF24L01+



- OLED or 2-Digit, 7 Segment Display



- Channel Switching using a rotary encoder or potentiometer



- Basic circuit with battery and enclosure plans


