#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
RF24 radio(9, 10); // CE, CSN

void setup() {
  radio.begin();
  radio.setPALevel(RF24_PA_HIGH);
  
  int wifiCenterChannels[] {
    2412, //Wifi Channel 1
    2417, // Ch. 2
    2422, // Ch. 3
    2427, // Ch. 4
    2432, // Ch. 5
    2437, // Ch. 6
    2442, // Ch. 7
    2447, // Ch. 8
    2452, // Ch. 9
    2457, // Ch. 10
    2462, // Ch. 11
    2467, // Ch. 12 (Avoided in NA)
    2472, // Ch. 13 (Avoided in NA)
    2484 // Ch. 14 (Japan Only)
  };

  int wifiChannel1Center = 2412; // in MHz
  int nrfChannel = wifiChannel1Center - 2400; // 2400 MHz is the start for the nRF24L01+
  radio.setChannel(nrfChannel);
  radio.setAutoAck(false); // disable auto acknowledgment
  radio.stopListening();
}

void loop() {         
  const char text[] = "abc41123etrgh";
  if (!radio.write(&text, sizeof(text))) {
    // wait a bit if the previous transmission is still in progress
    delay(1);
  }
}